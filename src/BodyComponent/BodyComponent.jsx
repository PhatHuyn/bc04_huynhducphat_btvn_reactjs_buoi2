import React, { Component } from 'react'
import { DataGlasses } from '../dataGlasses';
import ArrayGlassesComponent from './ArrayGlassesComponent';
import ItemImageComponent from './ItemImageComponent';


export default class BodyComponent extends Component {
    state = {
        GlassesArr: DataGlasses,
        detailGlasses: DataGlasses[0],
        AddItem: [],
    };

    handleViTriGlasses = (glass) =>{

      let index = this.state.GlassesArr.findIndex((item) => {
        return item.id == glass;
      });

      index !== -1 &&
      this.setState({
        detailGlasses: this.state.GlassesArr[index],
      });
    };   

    handleAddItem = (glass) => {
      let cloneAddItem = [this.state.AddItem];

      let index = this.state.AddItem.findIndex((item) => {
        return item.id == glass.id;
      });
      
      if(index == -1){
        let themAdditem = { ...glass};
        cloneAddItem.push(themAdditem);
      }
      this.setState({
        AddItem: cloneAddItem,
      });
    };

  render() {
    console.log('detailGlasses: ', this.state.detailGlasses);
    console.log('AddItem: ', this.state.AddItem);
    return (
    <div>
        <ItemImageComponent addItem={this.state.AddItem}/>
        <ArrayGlassesComponent data={this.state.GlassesArr} handleViTriGlasses={this.handleViTriGlasses} handleAddItem={this.handleAddItem}/>
    </div>
    )
  }
}