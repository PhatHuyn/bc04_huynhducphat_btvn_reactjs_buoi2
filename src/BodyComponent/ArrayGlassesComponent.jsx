import React, { Component } from 'react'

export default class ArrayGlassesComponent extends Component {
  render() {
    return (
      <div className='container '>
        <div className='arry'>
          <div className='row' id='ArrayGlasses'>
              {this.props.data.map((item, index) => {
                return(
                  <div key={index} className='col-3'>
                    <a id='aimg' type='button' onClick={() => { 
                      this.props.handleViTriGlasses(item.id);
                      this.props.handleAddItem(item);
                     }}>
                      <img src={item.url} alt="" className='w-100'/>
                    </a>
                  </div>
                ); 
              })}
          </div>
        </div>
      </div>
    );
  }
}
