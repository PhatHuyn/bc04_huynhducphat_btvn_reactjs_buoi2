import React, { Component } from 'react'
import imgface from '../Image/model.jpg';

export default class ItemImageComponent extends Component {
    renderImg = () => {
        return this.props.addItem.map((item, index) => {
            // document.getElementById("kinh1").style.display = "block";
            return(
                <img id='kinh' src={item.url}/>
            );
        });
    };

    renderTitle = () => {
        return this.props.addItem.map((item, index) => {
            document.getElementById("dsglass").style.display = "block";
            return(
                <div key={index}>
                    <h3 style={{textAlign: "left", color: "blue", marginLeft: 20}}>{item.name}</h3>
                    <p style={{textAlign: "left", color: "white", fontSize: 20, marginLeft: 20}}>{item.desc}</p>
                </div>
            );
        });
    }

  render() {
    return (
        <div className='container'>
            <div id='imgall'>
                <div id='Faceimg'>
                    <div id='Faceimg1'>
                        <img src={imgface} alt="" />
                        <div id='kinh1'>
                            {this.renderImg()}
                            <div id='dsglass'>
                            {this.renderTitle()}
                            </div>
                        </div>
                    </div>
                    <img src={imgface} alt="" />
                </div>
            </div>
        </div>
    )
  }
}
