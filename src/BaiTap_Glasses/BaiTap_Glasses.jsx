import React, { Component } from 'react'
import BodyComponent from '../BodyComponent/BodyComponent'
import HeaderComponent from '../HeaderComponent/HeaderComponent'
import styles from './BaiTap_Glasses.module.css'

export default class BaiTap_Glasses extends Component {
  render() {
    return (
    <div>
      <h1 className={styles.headercomponent}>
        <HeaderComponent />
      </h1>
      <div className={styles.bodycomponent}>
        <BodyComponent />
      </div>      
      </div>
    )
  }
}
